import { NextPage } from 'next'
import Link from 'next/link'

const Page: NextPage = () => (
    <main>
        <p>Login</p>
        <div>
            <Link href="/staff">
                Staff Portal
            </Link>
        </div>
        <div>
            <Link href="/organization">
                Organization Portal
            </Link>
        </div>
    </main>
)

export default Page
