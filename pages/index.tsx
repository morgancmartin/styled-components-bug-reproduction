import { NextPage } from 'next'
import Link from 'next/link'
import { PrimaryButton } from 'office-ui-fabric-react'
import styled from 'styled-components'

interface Props {
  userAgent?: string;
}

/* const Button = ({ className, children }) => (
 *   <div className={className}>
 *     <PrimaryButton>
 *       {children}
 *     </PrimaryButton>
 *   </div>
 * ) */

const Button = styled.button`
  background: palevioletred;
  border-radius: 3px;
  border: none;
  color: white;
  padding: 1rem;
`

const TomatoButton = styled(Button)`
  background: tomato;
`

const Page: NextPage<Props> = ({ userAgent }) => (
  <main>
    <p>Your user agent: {userAgent}</p>
    <Link href="/login">
      <TomatoButton>
        Login
      </TomatoButton>
    </Link>
  </main>
)

Page.getInitialProps = async ({ req }) => {
  const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
  return { userAgent }
}

export default Page
