* Project is configured to support SSR. Of particular note is the setting of the
  ssr field in `.babelrc` to `true`
  
* Styles load correctly on initial page load, however any subsequent changes to
  styled-components styles will cause all styled-components styles to disappear
  from the page. Manually reverting any changes to match the initial working
  state causes styled-components styles to begin working again just as they did
  on initial page load.

* Try changing the `TomatoButton` component's background from `tomato` to `blue`
  to see this in action.

* Changing `"styled-components"` version in `package.json` from `"^4.4.1"` to
  `"^5.0.0-rc.3"` fixes the issue.

* Environment Notes
* Node Version: v12.14.1
* OS: Manjaro Linux
* Package Manager: Yarn
